#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct dict {
	char *word;
	char *meaning;
};

struct node {
	struct dict *dict;
	struct node *next;
}*head, *tail;

void insert() {
	struct node *node = malloc(sizeof(struct node));

	struct dict *new = malloc(sizeof(struct dict));

	new->word = malloc(100);
	new->meaning = malloc(100);

	printf("Enter word: ");
	scanf("%s", new->word);

	printf("Enter meaning: ");
	scanf("%s", new->meaning);

	node -> dict = new;
	node -> next = NULL;

	if(head == NULL){
		head = node;
		tail = node;
	}else {
		tail->next = node;
		tail = node;
	}
}

void search() {
	struct node *current = malloc(sizeof(struct node));
	char wd[100];
	printf("Enter word to search: ");
	scanf("%s", &wd);

	current = head;
	while(current != NULL) {
		if(strcmp(current->dict->word, wd)==0) {
			printf("The word is: %s => %s \n", current->dict->word, current->dict->meaning);
		}
		current = current->next;
	}
}

void delete() {
	struct node *prev_word = malloc(sizeof(struct node));
	struct node *current = malloc(sizeof(struct node));

	char wd[100];
	printf("Enter word to delete: ");
	scanf("%s", &wd);

	prev_word = NULL;
	current = head;
	
	if(head == NULL){
		printf("emptylist\n");
	}else {
		// struct node* current = head;
		while(current != NULL) {
			if(strcmp(current->dict->word, wd)==0) {
				// printf("%s => %s", current->dict->word, current->dict->meaning);
				if(prev_word == NULL){
					prev_word->next = current->next;
					free(current);
				}
			}
				// else {
				// 	prev_word = current->next;
				// 	free(current);
				// 	// return head;
				// }
			}
		prev_word = current;
		current = current->next;
	}

	// printf("%s => %s", current->dict->word, current->dict->meaning);
	// 		current = current->next;
	// 		if(current != NULL)
	// 			printf("->\n");
	// 	}
	// 	printf("\n");
}




void display() {
	if(head == NULL){
		printf("emptylist\n");
	}else {
		struct node* current = head;
		while(current != NULL) {
			printf("%s => %s", current->dict->word, current->dict->meaning);
			current = current->next;
			if(current != NULL)
				printf("->\n");
		}
		printf("\n");
	}
}

void main(int argc, char const *argv[]){
	// char word[100];
	int choice;
	// int total_words, i;
	// printf("Enter total number of words: ");
	// scanf("%d", &total_words);

	// for(i = 0; i<total_words; i++){
	// 	struct dict *new = malloc(sizeof(struct dict));
	// 	new->word = malloc(100);
	// 	new->meaning = malloc(100);

	// 	printf("Enter %dth word: ", i);
	// 	scanf("%s", new->word);

	// 	printf("Enter %dth meaning: ", i);
	// 	scanf("%s", new->meaning);

	// 	insert(new);
	// }

	// struct dict *new = malloc(sizeof(struct dict));

	while(true) {
		printf("1.INSERT\n2.DELETE\n3.SEARCH\n4.DISPLAY\n5.EXIT\nEnter your choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: 
					// new->word = malloc(100);
					// new->meaning = malloc(100);

					// printf("Enter word: ");
					// scanf("%s", new->word);

					// printf("Enter meaning: ");
					// scanf("%s", new->meaning);

					insert();
					break;

			case 2: delete();
					break;

			case 3:	search();
					break;

			case 4: display();
					break;

			case 5: exit(0);
					break;
		}		
	}
}
	// display();
	// return 0;
