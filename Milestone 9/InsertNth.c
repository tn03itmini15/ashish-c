#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
void InsertNth(struct List** head_ref, int index, int data) 
{ 

   if(index==0) {
       push(head_ref,data);
   }
   else{
       struct List* current = *head_ref;
       int i;
       for(i=0; i<index-1;i++){
           assert(current !=NULL);
           current = current->next;
       }
       
       assert(current != NULL);
       
       push(&(current->next), data);
   }
} 
  
int main() 
{ 
    struct List* head = NULL; 
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1);  
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
     
    InsertNth(&head, 3, 4) ;
     
    printf("The data 4 is inserted at index 3" ); 
} 